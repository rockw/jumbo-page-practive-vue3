1. 處理 vuelidate 正則表達式邏輯
2. 跟註冊頁面做串接

## 註冊頁面流程 pseudocode

1. 按下一步

2. 若資料錯誤或為 null 變紅框
## 父組件
```jsx
const userInputBlock = ref(null)
const passwordInputBlock = ref(null)
const checkedPasswordInputBlock = ref(null)

function isDataGood: (El, data) => {
    變數需要
        1. El(邊界顏色)
        2. data

    let result = true
    檢查資料(null, length, format)
        1. OK => result = true 
        2. !OK => result = false

        if(data == 'a') {
            if(data == null) => result = false
            if(length !>= 2) => result = false 
            if(type != number) => result = false
        }
        if(data == 'b') {
            length == 3
            type = chinese
        }
        
    El.value.style.border = result ? "solid 1px rbga(0, 0, 0, 0.2)" : "solid 1px red"
    return result
}

function setStep(num) {
    clickStepStatus[nowStep] = true
    if(nowStep == 1) {
        if(!isDataGood(userInputBlock, data.login.username)) return false
        if(!isDataGood(passwordInputBlock, data.login.password)) return false
        if(!isDataGood(checkedPasswordInputBlock, data.login.checkedPassword)) return false
    }

    if(nowStep == 2) {
        ...
        ...
        ...

    }

    if(nowStep == 3) {
        ...
        ...
        ...
    }

    setStep(num)
}


return {
    userInputBlock,
    passwordInputBlock,
    checkedPasswordInputBlock,
    isDataGood

}
```
### 子組件


```jsx
<template>
name)")

</template>

props: [ userInputBlock, passwordInputBlock, checkedPasswordInputBlock, isDataGood ]
setup() {
    function isDataStillGood(El, data) {
        if(clickStepValue[1]) {
            isDataGood(El, data)
        }
    }
    
    return {
        isDataStillGood
    }
}

```
