import { createApp } from 'vue'
import App from './App.vue'
import './assets/style.sass'


createApp(App).mount('#app')
